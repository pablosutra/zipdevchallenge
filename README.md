This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Installation
## You'll need:
 - npm version >= 8.16.0
 - yarn (optional):  you can install yarn by running on the terminal `npm install -g yarn`

## Before Start
Before starting the application, you need to install dependencies, you can accomplish it by typing on the terminal `yarn` or `npm install` 

## Running the application
To run the application you can run the following command on terminal: `yarn start` or ` npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
ptimizes the build for the best performance.

##  Important to notice that was used on this challenge:
 - [Bulma framework](https://bulma.io/) - this was for faster prototyping and CSS styling.
 - [ramda](https://ramdajs.com/) - this for data manipulation
 
## Missing things and known issues:
- On Pizza listing the styling is not so accurate.
- Refactor on some code, most of the code is repeating constantly.
- Questions approach could be cleaner.
- For matter of time, no Redux was used (would be nice if added).