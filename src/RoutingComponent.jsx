import React, { Fragment, useEffect, useState } from 'react';
import { Switch, Route, Link, Redirect } from 'react-router-dom';

import { propEq, find, path, pluck } from 'ramda';
import axios from 'axios';

import { DEFAULT_URL, PIZZA_HEADERS, REQUESTED_REST_INFO } from './constants';

import Home from './components/Home';
import Quiz from './components/Quiz';
import Pizza from './components/Pizza';

const { city, city_complete, restaurantType } = REQUESTED_REST_INFO;

const RoutingComponent = () => {
    const [restaurants, setRestaurants] = useState([]);

    useEffect(() => {
        _getPizzaPlaces();
    }, []);

    //Getting Pizza info, to work on the background
    const _getPizzaPlaces = async () => {
        const cityReq = await axios.get(`${DEFAULT_URL}/cities`, {
            params: { q: city },
            headers: PIZZA_HEADERS,
        });
        const {
            data: { location_suggestions },
        } = cityReq;
        const cityRes = find(
            propEq('name', city_complete),
            location_suggestions,
        );
        const cityId = path(['id'], cityRes);
        if (cityId) {
            const cuisineReq = await axios.get(`${DEFAULT_URL}/cuisines`, {
                params: { city_id: cityId },
                headers: PIZZA_HEADERS,
            });
            const {
                data: { cuisines },
            } = cuisineReq;
            const cuisinesList = pluck('cuisine', cuisines);
            const cuisineRes = find(
                propEq('cuisine_name', restaurantType),
                cuisinesList,
            );
            const cuisineId = path(['cuisine_id'], cuisineRes);
            if (cuisineId) {
                const restaurantReq = await axios.get(`${DEFAULT_URL}/search`, {
                    params: {
                        entity_id: cityId,
                        entity_type: 'city',
                        cuisines: cuisineId,
                    },
                    headers: PIZZA_HEADERS,
                });
                const {
                    data: { restaurants },
                } = restaurantReq;
                setRestaurants(restaurants);
            }
        }
    };
    return (
        <Fragment>
            <nav
                className="navbar is-primary m-b-30"
                role="navigation"
                aria-label="main navigation"
            >
                <div className="navbar-menu">
                    <div className="navbar-start">
                        <Link className="navbar-item" to="/quiz">
                            Take the Quiz
                        </Link>
                        <Link className="navbar-item" to="/pizza">
                            Pizza
                        </Link>
                    </div>
                </div>
            </nav>
            <div className="container is-container">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/quiz" component={Quiz} />
                    <Route
                        exact
                        path="/pizza"
                        component={() => <Pizza restaurants={restaurants} />}
                    />
                    <Redirect to="/" />
                </Switch>
            </div>
        </Fragment>
    );
};
export default RoutingComponent;
