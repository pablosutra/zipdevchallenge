import React from 'react';

const Home = () => (
    <article className="message is-dark">
        <div className="message-header">
            <p>Welcome</p>
        </div>
        <div className="message-body">
            Please select on the menu bar above, if you'd like to take a quiz,
            or order a pizza.
        </div>
    </article>
);

export default Home;
