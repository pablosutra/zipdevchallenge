import React, { Fragment } from 'react';
import { array } from 'prop-types';

import Loading from '../common/Loading';

import { REQUESTED_REST_INFO } from '../../constants';
import './styles.scss';

const { city_complete } = REQUESTED_REST_INFO;
const Pizza = ({ restaurants }) => {
    if (restaurants.length) {
        return (
            <Fragment>
                <h2 className="title">Order a Pizza</h2>
                <h3 className="subtitle">
                    Select your favorite {city_complete} restaurant
                </h3>
                <div className="m-t-30">
                    {restaurants.map(res => {
                        const { restaurant } = res;
                        const {
                            R,
                            thumb,
                            name,
                            location: { locality },
                            establishment,
                            timings,
                            cuisines,
                            average_cost_for_two,
                            user_rating,
                            all_reviews_count,
                        } = restaurant;
                        return (
                            <section
                                className="restaurant-container m-t-20"
                                key={R.res_id}
                            >
                                <div className="restaurant-details">
                                    <img
                                        className="image is-128x128"
                                        src={thumb}
                                        alt="restaurant image"
                                    />
                                    <p className="restaurant-name subtitle has-text-weight-bold">
                                        {name}
                                    </p>
                                    <div className="restaurant-info is-size-7">
                                        <p>
                                            {locality}{' '}
                                            {establishment
                                                ? `- ${establishment.join(',')}`
                                                : ''}
                                        </p>
                                        <div className="restaurant-hours">
                                            <p className="restaurant-timings">
                                                <span className="has-text-primary">
                                                    {timings}
                                                </span>{' '}
                                                -
                                                <span className="has-text-danger">
                                                    {cuisines}
                                                </span>{' '}
                                                - cost ${average_cost_for_two}{' '}
                                                for two
                                            </p>
                                        </div>
                                    </div>
                                    <div className="restaurant_ratings">
                                        <p
                                            className="tag is-large has-text-white"
                                            style={{
                                                backgroundColor: `#${user_rating.rating_color}`,
                                            }}
                                        >
                                            {user_rating.aggregate_rating}{' '}
                                            <span className="is-size-7">
                                                /5
                                            </span>
                                        </p>
                                        <p className="is-size-7 has-text-centered has-text-grey">
                                            {all_reviews_count} votes
                                        </p>
                                    </div>
                                </div>
                            </section>
                        );
                    })}
                </div>
            </Fragment>
        );
    }
    return <Loading />;
};

Pizza.propTypes = {
    restaurants: array,
};

export default Pizza;
