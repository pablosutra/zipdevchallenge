import React, { useState, Fragment } from 'react';
import { merge, includes, intersection } from 'ramda';
import classNames from 'classnames';
import { questions } from './questions';
import getTruthyKeys from '../../helpers/getTruthyKeys';

const Quiz = () => {
    const [currentQuestion, setCurrentQuestion] = useState(1);
    const [answers, setAnswers] = useState([]);
    const totalQuestions = questions.length;

    const startQuiz = () => {
        setAnswers([]);
        setCurrentQuestion(1);
    };
    const renderQuestion = (question, questionNumber) => {
        let answer;
        const setMultipleAnswer = ({ target: { checked } }, option) => {
            answer = merge(answer, { [option]: checked });
            answers[questionNumber] = answer;
            setAnswers(answers);
        };
        const setSingleAnswer = ev => {
            answers[questionNumber] = ev.target.value;
            setAnswers(answers);
        };
        const setMultipleText = (ev, option) => {
            answer = merge(answer, { [option]: ev.target.value });

            answers[questionNumber] = answer;
            setAnswers(answers);
        };

        if (question.type === 'multipleChoice') {
            return question.options.map(option => (
                <label
                    className="checkbox is-block m-l-10 m-t-10 m-b-10"
                    key={Math.random()}
                >
                    <input
                        type="checkbox"
                        onChange={ev => setMultipleAnswer(ev, option)}
                    />
                    {option}
                </label>
            ));
        }
        if (question.type === 'singleChoice') {
            return (
                <div className="control">
                    {question.options.map(option => (
                        <label
                            className="radio is-block m-l-10 m-t-10 m-b-10"
                            key={Math.random()}
                        >
                            <input
                                type="radio"
                                name={questionNumber}
                                value={option}
                                onChange={setSingleAnswer}
                            />
                            {option}
                        </label>
                    ))}
                </div>
            );
        }

        if (question.type === 'text') {
            return (
                <input
                    type="text"
                    className="input m-t-10 m-b-10"
                    placeholder="Answer"
                    onChange={setSingleAnswer}
                />
            );
        }
        if (question.type === 'multipleText') {
            return (
                <Fragment>
                    <input
                        type="text"
                        className="input m-t-10 m-b-10"
                        placeholder="Answer"
                        onChange={ev => setMultipleText(ev, 'normal')}
                    />
                    <input
                        type="text"
                        className="input m-t-10 m-b-10"
                        placeholder="Answer in backwards"
                        onChange={ev => setMultipleText(ev, 'backwards')}
                    />
                </Fragment>
            );
        }
        return null;
    };
    const renderResults = () => {
        return questions.map((question, idx) => {
            let currentAnswer = answers[idx];
            if (
                answers[idx] instanceof Object &&
                question.type !== 'multipleText'
            ) {
                currentAnswer = getTruthyKeys(answers[idx]);
            }
            if (
                answers[idx] instanceof Object &&
                question.type === 'multipleText'
            ) {
                currentAnswer = Object.values(answers[idx]);
            }

            const isCorrectAnswer = () => {
                const { correctAnswer } = question;
                if (correctAnswer) {
                    if (correctAnswer instanceof Function) {
                        if (currentAnswer instanceof Array) {
                            return correctAnswer.apply(this, currentAnswer);
                        }
                        return correctAnswer(currentAnswer);
                    }
                    if (correctAnswer instanceof Array) {
                        const intersected = intersection(
                            currentAnswer,
                            correctAnswer,
                        );
                        return intersected.length === correctAnswer.length;
                    }
                }
                return true;
            };
            const resultClass = classNames('subtitle has-text-white', {
                'has-background-danger': !isCorrectAnswer(),
                'has-background-success': isCorrectAnswer(),
            });
            if (question.options) {
                return (
                    <Fragment>
                        <p className={resultClass}>
                            {idx + 1}.- {question.text}
                        </p>
                        <ul>
                            {question.options.map(option => {
                                const highlightClasses = classNames({
                                    'has-background-info has-text-white': includes(
                                        option,
                                        currentAnswer,
                                    ),
                                });
                                return (
                                    <li
                                        key={Math.random()}
                                        className={highlightClasses}
                                    >
                                        {option}
                                    </li>
                                );
                            })}
                        </ul>
                        <hr />
                    </Fragment>
                );
            }
            return (
                <Fragment>
                    <p className={resultClass}>
                        {idx + 1}.- {question.text}
                    </p>
                    {question.type === 'text' && (
                        <p className="is-size-5">{currentAnswer}</p>
                    )}
                    {question.type === 'multipleText' && (
                        <Fragment>
                            {currentAnswer.map(ans => (
                                <p key={Math.random()}>{ans}</p>
                            ))}
                        </Fragment>
                    )}
                </Fragment>
            );
        });
    };

    return (
        <div className="m-t-30">
            <h2 className="title">Quiz Time!</h2>
            <progress
                className="progress is-primary"
                value={currentQuestion}
                max={totalQuestions}
            >
                {currentQuestion} / totalQuestions
            </progress>
            {questions.map((question, idx) => {
                const questionClasses = classNames('message', {
                    'is-hidden': currentQuestion !== idx + 1,
                });
                return (
                    <div className={questionClasses} key={idx}>
                        <div className="message-header">
                            <p>{question.text}</p>
                        </div>
                        <div className="message-body">
                            {renderQuestion(question, idx)}
                            <button
                                className="button is-primary m-t-10"
                                onClick={() =>
                                    setCurrentQuestion(currentQuestion + 1)
                                }
                            >
                                Answer
                            </button>
                        </div>
                    </div>
                );
            })}
            {currentQuestion > totalQuestions && renderResults()}
            <hr />
            <button className="button is-primary" onClick={startQuiz}>
                Start Over{' '}
            </button>
        </div>
    );
};

export default Quiz;
