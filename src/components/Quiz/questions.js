export const questions = [
    {
        text: 'How do you describe yourself as a developer?',
        type: 'singleChoice',
        options: ['Hermit', 'Sociable', 'Serious', 'Grumpy', 'Do not know yet'],
        correctAnswer: null,
    },
    {
        text: 'Select the JavaScript based technologies:',
        type: 'multipleChoice',
        options: ['AngularJS', 'Ember', 'VueJS', 'Java', 'C#'],
        correctAnswer: ['AngularJS', 'Ember', 'VueJS'],
    },
    {
        text:
            'A palindrome is a word, number, phrase, or other sequence of characters which reads the same backward as forward, please write a palinfrome',
        type: 'text',
        correctAnswer: text => {
            if (!text) return false;
            return (
                text.replace(/\s/g, '') ===
                text
                    .replace(/\s/g, '')
                    .split('')
                    .reverse()
                    .join('')
            );
        },
    },
    {
        text:
            'Please write one word, phrase or number on the first box, and on the second box, write the same word, phrase or number backwards',
        type: 'multipleText',
        boxes: 2,
        correctAnswer: (a, b) => {
            if (!a || !b) return false;
            return (
                a
                    .replace(/\s/g, '')
                    .split('')
                    .reverse()
                    .join('') ===
                b
                    .replace(/\s/g, '')
                    .split('')
                    .join('')
            );
        },
    },
];
