export const DEFAULT_URL = 'https://developers.zomato.com/api/v2.1/';

export const PIZZA_HEADERS = {
    'user-key': '85f007936513e1772ad651255ad907c8',
};

export const REQUESTED_REST_INFO = {
    city: 'san diego',
    city_complete: 'San Diego, CA',
    restaurantType: 'Pizza',
};
