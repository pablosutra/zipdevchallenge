/**
 * Gets all the keys on an object whose value is thruty
 * @param {Object} obj
 * @return {Array} arr
 */
function getTruthyKeys(obj) {
    return Object.entries(obj).reduce((arr, [key, val]) => {
        if (val) arr.push(key);
        return arr;
    }, []);
}

export default getTruthyKeys;
