import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import * as serviceWorker from './serviceWorker';
import './index.scss';

import RoutingComponent from './RoutingComponent';

const history = createBrowserHistory();

ReactDOM.render(
    <Router history={history}>
        <RoutingComponent />
    </Router>,
    document.getElementById('app'),
);
serviceWorker.unregister();
